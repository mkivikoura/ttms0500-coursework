class MatchviewerApp extends React.Component {
    constructor(props) {
        super(props);
        this.state = { matches: [], teams: []};
    }

    getData = (dataURL, objectKey) => {
        $.ajax({
            url: dataURL,
            cache: false,
            dataType: 'json'
        }).done((data) => {
            var newObject = {};
            newObject[objectKey] = data[objectKey];
            this.setState(newObject);
        }).fail((jqXHR, textStatus, errorThrown) => {
            console.log(textStatus + ": " + errorThrown);
        });
    };

    updateData = (data,) => {
        this.setState(data);
    };

    componentDidMount = () => {
        this.getData('js/matches.json', 'matches');
        this.getData('js/teams.json', 'teams');
    };

    render = () => {
        return (
            <div>
                <MatchList matches={this.state.matches} updateData={this.updateData} />
                <TeamList teams={this.state.teams} updateData={this.updateData} />
                <PlayerList teams={this.state.teams} updateData={this.updateData} />
            </div>
        );
    };
}

class MatchList extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div class="section">
                <div className="container">
                    <h1 class="title">Matches</h1>
                    <table className="table">
                        <tbody>
                        {
                            this.props.matches.map((match, index) =>
                                <tr key={index}>
                                    <td>{this.formatDate(match.datetime)}</td>
                                    <td className="has-text-right">{match.teams.home.name}</td>
                                    <td className="has-text-centered">
                                        {match.teams.home.result} : {match.teams.guest.result}
                                    </td>
                                    <td className="has-text-left">{match.teams.guest.name}</td>
                                </tr>
                            )
                        }
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }

    formatDate(datetime) {
        return new Date(datetime).toLocaleString();
    }

}

class TeamList extends React.Component {
    constructor(props) {
        super(props);
        this.state = { newTeam: null };
    }

    render() {
        return (
          <div class="section">
              <div className="container">
                  <h1 class="title">Teams</h1>
                  <ul>
                      {
                          this.props.teams.map((team, index) =>
                            <li key={index}>{team.name}</li>
                          )
                      }
                      <li class="field has-addons">
                          <div class="control">
                              <input type="text" class="input" value={this.state.newTeam} onChange={this.handleChange}/>
                          </div>
                          <div class="control">
                              <a class="button" onClick={this.registerTeam}>+</a>
                          </div>
                      </li>
                  </ul>
              </div>
          </div>
        );
    }

    handleChange = (event) => {
        this.setState({newTeam: event.target.value});
    };

    registerTeam = (event) => {
        let newTeamsArr = this.props.teams.slice();
        newTeamsArr.push({name: this.state.newTeam, players: new Array(5)});
        this.props.updateData({teams: newTeamsArr});
        this.setState({newTeam: ""});

    };
}

class PlayerList extends React.Component {
    constructor(props) {
        super(props);
        this.state = { teamInEdit: -1, editedPlayers: []} ;
    }

    listPlayers = (teamIndex) => {
        let players = [];

        for (var i = 0; i < this.props.teams[teamIndex].players.length; i++) {
            if (teamIndex == this.state.teamInEdit) {
                players[i] = (
                <td key={i}>
                    <input id={i}
                           type="text"
                           className="input"
                           value={this.state.editedPlayers[i]}
                           onChange={this.handleChange}/>
                </td>
                );
            } else {
                players[i] = <td key={i}>{this.props.teams[teamIndex].players[i]}</td>;
            }
        }

        return players;
    };

    handleChange = (event) => {
        let newPlayersArr = this.state.editedPlayers.slice();
        newPlayersArr[event.target.id] = event.target.value;
        this.setState({editedPlayers: newPlayersArr});
    };

    inputControl = (teamIndex) => {
        if (teamIndex == this.state.teamInEdit) {
            let newTeamsArr = this.props.teams.slice();
            newTeamsArr[teamIndex].players = this.state.editedPlayers;
            this.props.updateData({teams: newTeamsArr});
            this.setState({teamInEdit: -1, editedPlayers: new Array(5)});
        }
        else {
            this.setState({editedPlayers: this.props.teams[teamIndex].players, teamInEdit: teamIndex });
        }
    };

    render() {
        return(
            <div class="section">
                <div class="container">
                    <h1 class="title">Players</h1>
                        <table class="table is-hoverable column is-three-quarters">
                            <thead>
                                <tr>
                                    <td>Team</td>
                                    <td>Core (1)</td>
                                    <td>Core (2)</td>
                                    <td>Offlaner (3)</td>
                                    <td>Support (4)</td>
                                    <td>Support (5)</td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                            {
                                this.props.teams.map((team, teamIndex) =>
                                    <tr key={teamIndex}>
                                        <td>{team.name}</td>
                                        {
                                            this.listPlayers(teamIndex)
                                        }
                                        <td>
                                            <div className="control">
                                                <a className="button is-small"
                                                   onClick={(e) => this.inputControl(teamIndex)}>
                                                    <ion-icon name="create"></ion-icon>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                )
                            }
                            </tbody>
                        </table>
                </div>
            </div>
        );
    }
}

ReactDOM.render(
    <MatchviewerApp/>,
    document.getElementById("app")
);